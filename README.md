# App helps to train in fast solving Rubik's cube puzzle 
## You can choose amount of time, press start and then app will start to countdown. If you are able to solve your analog Rubik's cube puzzle until time ends, you will win, otherwise everything will blow up :D

![](screens/app.gif)

# Technical aspects:
## Using webView, operations on svg, interractions beteween svg and native android via js bridge.


## Screenshots

![](screens/screen1.png) ![](screens/screen2.png)
![](screens/screen3.png) ![](screens/screen4.png)
![](screens/screen5.png)
