package katarzyna.showanimation;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
{
    WebView webViewAnimation;
    MediaPlayer mediaPlayer;
    Scheduler scheduler;
    int elapsedTimeSeconds;
    int clickCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webViewAnimation = findViewById(R.id.webViewAnimation);

        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
           int minutes = extras.getInt("minutes", 0);
           int seconds =extras.getInt("seconds", 10);

            elapsedTimeSeconds = minutes * 60 + seconds;
        }
        else elapsedTimeSeconds = 10;

        clickCounter = 1;

        // zezwolenie na js
        webViewAnimation.getSettings().setJavaScriptEnabled(true);

        // most java <-> js
        webViewAnimation.addJavascriptInterface(new WebAppInterface(this), "android");
        webViewAnimation.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                view.loadUrl("javascript: (function() {document.getElementById('HJSarnxhz').addEventListener('click', function() {android.cubeResolver(\"Ułóż kostkę zanim wybuchnie bomba!\nDotknij kostkę jeśli skończysz\");}); })();");
            }
        });

        webViewAnimation.loadUrl("file:///android_asset/cube.svg");
    }

    public class WebAppInterface
    {
        Context mContext;

        WebAppInterface(Context c)
        {
            mContext = c;
        }

        @JavascriptInterface
        public void cubeResolver(final String toast)
        {
            if(clickCounter%2==0 && clickCounter>0)
            {
                scheduler.timerCancel();
                clickCounter = 1;

                Toast.makeText(mContext, "Uff... Bomba nie wybuchła! GRATULACJE", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();

                clickCounter++;

                mediaPlayer = MediaPlayer.create(mContext,R.raw.beep);
                mediaPlayer.start();
                mediaPlayer.setLooping(true);

                scheduler = new Scheduler(elapsedTimeSeconds);
            }
        }
    }

    public class Scheduler
    {
        Timer timer;

        public Scheduler(int seconds)
        {
            timer = new Timer();
            timer.schedule(new RemindTask(), seconds*1000);
        }

        public void timerCancel()
        {
            timer.cancel();
            mediaPlayer.setLooping(false);
        }

        class RemindTask extends TimerTask
        {
            public void run()
            {
                mediaPlayer.setLooping(false);
                mediaPlayer.stop();

                Intent intent = new Intent(MainActivity.this, Summary.class);
                startActivity(intent);

                timer.cancel(); //Wyłączamy taska
            }
        }
    }
}
