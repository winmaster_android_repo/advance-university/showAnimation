package katarzyna.showanimation;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Summary extends AppCompatActivity
{
    WebView webViewExplosion;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        webViewExplosion = findViewById(R.id.webViewExplosion);

        // zezwolenie na js
        webViewExplosion.getSettings().setJavaScriptEnabled(true);

        // most java <-> js
        webViewExplosion.addJavascriptInterface(new Summary.WebAppInterface(this), "android");
        webViewExplosion.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                view.loadUrl("javascript: (function() {document.getElementById('SJT4XTvnM').addEventListener('click', function() {android.gameOver(\"Game Over!\");}); })();");
            }
        });

        webViewExplosion.loadUrl("file:///android_asset/explosion.svg");

        mediaPlayer = MediaPlayer.create(this, R.raw.explode);
        mediaPlayer.start();
    }

    public class WebAppInterface
    {
        Context mContext;

        WebAppInterface(Context c)
        {
            mContext = c;
        }

        @JavascriptInterface
        public void gameOver(final String toast)
        {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed()
    {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
