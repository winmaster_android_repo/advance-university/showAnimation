package katarzyna.showanimation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;

public class Timer extends AppCompatActivity
{
    TimePicker timePicker;

    int minutes=0;
    int seconds=10;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        timePicker = findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        timePicker.setHour(0);
        timePicker.setMinute(10);

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener()
        {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1)
            {
                minutes = timePicker.getHour();
                seconds = timePicker.getMinute();
            }
        });
    }

    public void goNext(View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("minutes", minutes);
        intent.putExtra("seconds", seconds);
        startActivity(intent);
    }
}
